---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
# library(readr)
```

```{r development-load}
# Load already included functions if relevant
# pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok
    
```{r function-check_primary_color_is_ok}
#' Check color in a vector 
#' 
#' Check color in vector
#' 
#' @param string vector or character. A color
#' 
#' @return Boolean. TRUE if all color are correct
#' 
#' @export
check_primary_color_is_ok <- function(string) {
  
  
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
  if (isFALSE(all_colors_OK)) {
    stop("Toutes les couleurs ne sont pas ok")
  }
  
  return(all_colors_OK)
}
```
  
```{r example-check_primary_color_is_ok}
check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
check_primary_color_is_ok(string = c("Gray", "Cinnamon"))
# check_primary_color_is_ok(string = c("Gray", "Blue"))
```
  
```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
  )

  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black"))
  )
  
  expect_error(
    object = check_primary_color_is_ok(string = c("Gray", "Blue")),
    regexp = "Toutes les couleurs ne sont pas ok"
  )
})
```


# check_squirrel_data_integrity
    
```{r function-check_squirrel_data_integrity}
#' check_squirrel_data_integrity
#' 
#' Vérifie la présence de la colonne et les couleurs dans le jeu de données en param.
#' 
#' @param data_act_squirrels dataset on squirrels
#' @return rien. Seulement un message dans la console
#' 
#' @export
check_squirrel_data_integrity <- function(data_act_squirrels){
  
  if (any(colnames(data_act_squirrels) == "primary_fur_color")) {
    if (check_primary_color_is_ok(data_act_squirrels$primary_fur_color)) {
      message("Les donnees contiennent bien la colonne primary_fur_color. Et il n'y a que des couleurs autorisees. Cool !")
    }
    else{
      stop("Il y a des couleurs INTERDITES !")
    }
  }
  else{
    stop("DANGER : pas de colonne primary_fur_color dans le jeu de donnees")
  }
}
```
  
# ```{r example-check_squirrel_data_integrity}
# check_squirrel_data_integrity(data_act_squirrels)
# 
# # pkgload::load_all(path = "squirrelsromain")
#   
# le_chemin_des_data <- system.file("nyc_squirrels_sample.csv", package = "squirrelsromain")
# les_data <- readr::read_csv(le_chemin_des_data)
# check_squirrel_data_integrity(les_data)
# ```
  
```{r tests-check_squirrel_data_integrity}
test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function")) 
})
```
  


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_check_data.Rmd",
  vignette_name = "check-data"
)
```
